{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training embeddings with siamese models and adapted losses"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll have a look at how to train embeddings with siamese models and purpose-built loss functions.\n",
    "The approach can be applied to all kinds of inputs and models; we shall use mnist digits and shallow cnns instead of sentences and transformers to illustrate the main ideas and run examples in short time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "jupyter": {
     "outputs_hidden": false
    },
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import set_matplotlib_formats\n",
    "%matplotlib inline\n",
    "set_matplotlib_formats('svg')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Step 1: Preparing the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "We use the MNIST dataset and the library `tensorflow_datasets` which, coincidentally, underlies the `datasets` library of huggingface:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "jupyter": {
     "outputs_hidden": false
    },
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import tensorflow_datasets as tdfs\n",
    "\n",
    "tdfs.disable_progress_bar()\n",
    "\n",
    "mnist_train = tdfs.load(name='mnist', split='train')\n",
    "mnist_test = tdfs.load(name='mnist', split='test')\n",
    "\n",
    "mnist_train, mnist_test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For training embeddings, we want to have \n",
    "\n",
    "- pairs of digit images as input and \n",
    "- a label telling us whether the digits coincide or not.\n",
    "\n",
    "Such a dataset can be obtained by\n",
    "\n",
    "- forming batches of 2 consequtive mnist samples\n",
    "- mapping the following `process_sample_pair` function to batches:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def process_sample_pair(samples):\n",
    "    return ({'image_1': samples['image'][0], 'image_2': samples['image'][1]}, 1 if samples['label'][0] == samples['label'][1] else 0)\n",
    "\n",
    "train_pairs = mnist_train.batch(2).map(process_sample_pair)\n",
    "test_pairs = mnist_test.batch(2).map(process_sample_pair)\n",
    "\n",
    "for sample in train_pairs.take(10):\n",
    "    print(sample[1])\n",
    "    plt.figure(figsize=(3,6))\n",
    "    plt.imshow(np.concatenate(list(sample[0].values()), axis=1))\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build a model to produce embeddings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The next step is to build a model that we want to train for producing embeddings. We take a simple cnn with just one convolutional layer to produce 4-dimensional embeddings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_embedding_model():\n",
    "    return tf.keras.Sequential([\n",
    "        tf.keras.Input(shape=(28,28,1)),\n",
    "        tf.keras.layers.Conv2D(4, kernel_size=(3,3), strides=(1,1)),\n",
    "        tf.keras.layers.Flatten(),\n",
    "        tf.keras.layers.Dense(4),\n",
    "    ])\n",
    "\n",
    "embedding_model = build_embedding_model()\n",
    "embedding_model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build and train a siamese model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we build, given an embedding model, a siamese model that\n",
    "\n",
    "- takes two images as input\n",
    "- applies the same embedding model to both images\n",
    "- computes the cosine similarity of the embeddings obtained\n",
    "\n",
    "and take the output as a prediction for equality (1) or not (0)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_siamese_model(embedding_model):\n",
    "    inputs = [tf.keras.Input(shape=(28, 28, 1)) for _ in range(2)]\n",
    "    outputs = list(map(embedding_model, inputs))\n",
    "    similarity = tf.keras.layers.Dot(axes=1, normalize=True)(outputs)\n",
    "    siamese_model = tf.keras.Model(inputs=dict(zip(['image_1', 'image_2'], inputs)), outputs=[similarity])\n",
    "    return siamese_model\n",
    "\n",
    "siamese_model = build_siamese_model(embedding_model)\n",
    "siamese_model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When we train this siamese model, implicitly we train the embedding model to produce \n",
    "\n",
    "- similar embeddings for images showing the same digit\n",
    "- dissimilar embeddings for images showing different digits."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train(model, train_data, test_data, batch_size=64, epochs=2, loss=\"binary_crossentropy\"):\n",
    "    model.compile(optimizer=\"Adam\", loss=loss, metrics=\"accuracy\")\n",
    "    model.fit(train_data.batch(batch_size), epochs=epochs, validation_data=test_data.batch(batch_size))\n",
    "\n",
    "embedding_model = build_embedding_model()\n",
    "siamese_model = build_siamese_model(embedding_model)\n",
    "train(siamese_model, train_pairs, test_pairs)\n",
    "            "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize the embeddings, we \n",
    "\n",
    "- apply the embedding model to a large number of test images\n",
    "- project the embeddings to a plane using PCA\n",
    "- colorize the projected embeddings according to the digit on the image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import altair as alt\n",
    "from sklearn.decomposition import PCA\n",
    "\n",
    "def plot_embeddings(dataset, embedding_model, count=1000):\n",
    "    samples = dataset.take(count)\n",
    "    images = np.stack(samples.map(lambda sample: sample['image']))\n",
    "    labels = list(map(int, samples.map(lambda sample: sample['label'])))\n",
    "    embeddings = embedding_model.predict(images)\n",
    "    coordinates = PCA(2).fit_transform(embeddings)\n",
    "    df = pd.DataFrame(coordinates, columns=['x', 'y']).assign(label=labels)\n",
    "    return alt.Chart(df).mark_circle(opacity=0.5).encode(x='x', y='y', color='label:N')\n",
    "    \n",
    "plot_embeddings(mnist_test, embedding_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Batch siamese loss"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The training process can be sped up as follows:\n",
    "\n",
    "- apply the embedding model to a batch of input images\n",
    "- compute the pair-wise similarities of all embeddings in this batch\n",
    "- check the pair-wise equality of the labels in this batch\n",
    "- compute the binary cross-entropy for the pair-wise similarities and pair-wise equalities.\n",
    "\n",
    "Indeed, with a batch of $n$ examples, we process $\\frac{n(n-1)}{2}$ pairs instead of $2$!\n",
    "\n",
    "The nice thing is that steps 2-4 above can all be put into a loss function without touching the model.\n",
    "Let's do that step by step:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "embeddings = tf.constant([[1,0],[1,1],[0,1]], dtype=float)\n",
    "embeddings = tf.linalg.normalize(embeddings, axis=1)[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "embeddings"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "similarity = tf.matmul(embeddings, embeddings, transpose_b=True)\n",
    "similarity"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "labels = tf.constant([0,0,1])\n",
    "labels"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "label_equality = tf.math.equal(tf.expand_dims(labels, 0), tf.expand_dims(labels, 1))\n",
    "label_equality"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tf.where(label_equality, 1, 0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@tf.function\n",
    "def batch_siamese_loss(y_true, y_pred):\n",
    "    y_pred_normed, y_pred_norms = tf.linalg.normalize(y_pred, axis=-1)\n",
    "    similarity = tf.matmul(y_pred_normed, y_pred_normed, transpose_b=True)\n",
    "    label_equality = tf.math.equal(tf.expand_dims(y_true, 0), tf.expand_dims(y_true, 1))\n",
    "    targets = tf.where(label_equality, 1.0, 0.0)\n",
    "    similarity_flat = tf.reshape(similarity, [-1])\n",
    "    targets_flat = tf.reshape(targets, [-1])\n",
    "    return tf.losses.binary_crossentropy(targets_flat, similarity_flat)\n",
    "\n",
    "batch_siamese_loss(labels, embeddings)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now can train the model and evaluate the quality of the embeddings with a plot as above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Xy_train = mnist_train.map(lambda sample: tuple(sample.values()))\n",
    "Xy_test = mnist_test.map(lambda sample: tuple(sample.values()))\n",
    "\n",
    "embedding_model = build_embedding_model()\n",
    "train(embedding_model, Xy_train, Xy_test, loss=batch_siamese_loss)\n",
    "plot_embeddings(mnist_test, embedding_model)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@tf.function\n",
    "def batch_siamese_loss_alpha(y_true, y_pred, alpha=1e-3):\n",
    "    y_pred_normed, y_pred_norms = tf.linalg.normalize(y_pred, axis=-1)\n",
    "    norm_loss = tf.reduce_mean(tf.abs(y_pred_norms - tf.ones_like(y_pred_norms)))\n",
    "    similarity = tf.matmul(y_pred_normed, y_pred_normed, transpose_b=True)\n",
    "    label_equality = tf.math.equal(tf.expand_dims(y_true, 0), tf.expand_dims(y_true, 1))\n",
    "    targets = tf.where(label_equality, 1.0, 0.0)\n",
    "    similarity_flat = tf.reshape(similarity, [-1])\n",
    "    targets_flat = tf.reshape(targets, [-1])\n",
    "    return tf.losses.binary_crossentropy(targets_flat, similarity_flat) + norm_loss * alpha\n",
    "\n",
    "embedding_model = build_embedding_model()\n",
    "train(embedding_model, Xy_train, Xy_test, loss=batch_siamese_loss_alpha)\n",
    "plot_embeddings(mnist_test, embedding_model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hard/semihard negative mining"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `tensorflow_addons` library already provides an implementation of the `triplet loss` including online semi-hard or hard negative mining:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import tensorflow_addons as tfa\n",
    "\n",
    "help(tfa.losses.triplet_semihard_loss)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "embedding_model = build_embedding_model()\n",
    "train(embedding_model, Xy_train, Xy_test, loss=tfa.losses.triplet_semihard_loss)\n",
    "plot_embeddings(mnist_test, embedding_model)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  },
  "name": "02b_match_digits.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
